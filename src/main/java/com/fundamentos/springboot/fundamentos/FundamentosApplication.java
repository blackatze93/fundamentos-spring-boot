package com.fundamentos.springboot.fundamentos;

import com.fundamentos.springboot.fundamentos.bean.MyBean;
import com.fundamentos.springboot.fundamentos.bean.MyBeanWithDependency;
import com.fundamentos.springboot.fundamentos.bean.MyBeanWithProperties;
import com.fundamentos.springboot.fundamentos.bean.MyOperation;
import com.fundamentos.springboot.fundamentos.component.ComponentDependency;
import com.fundamentos.springboot.fundamentos.entity.User;
import com.fundamentos.springboot.fundamentos.pojo.UserPojo;
import com.fundamentos.springboot.fundamentos.repository.UserRepository;
import com.fundamentos.springboot.fundamentos.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class FundamentosApplication implements CommandLineRunner {

    private ComponentDependency componentDependency;
    private MyBean myBean;
    private MyBeanWithDependency myBeanWithDependency;
    private MyOperation myOperation;
    private MyBeanWithProperties myBeanWithProperties;
    private UserPojo userPojo;
    private final Log LOGGER = LogFactory.getLog(FundamentosApplication.class);
    private UserRepository userRepository;
    private UserService userService;

    public FundamentosApplication(@Qualifier("componentTwoImplement") ComponentDependency componentDependency,
                                  MyBean myBean,
                                  MyBeanWithDependency myBeanWithDependency,
                                  MyOperation myOperation,
                                  MyBeanWithProperties myBeanWithProperties,
                                  UserPojo userPojo,
                                  UserRepository userRepository,
                                  UserService userService) {
        this.componentDependency = componentDependency;
        this.myBean = myBean;
        this.myBeanWithDependency = myBeanWithDependency;
        this.myOperation = myOperation;
        this.myBeanWithProperties = myBeanWithProperties;
        this.userPojo = userPojo;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    public static void main(String[] args) {
        SpringApplication.run(FundamentosApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        ejemplosAnteriores();
//        saveUsersInDataBase();
//        getInformationJpqlFromUser();
        saveWithErrorTransactional();
    }

    public void saveWithErrorTransactional() {
        User test1 = new User("TestTransactional1", "TestTransactional1@domain.com", LocalDate.now());
        User test2 = new User("TestTransactional2", "TestTransactional2@domain.com", LocalDate.now());
        User test3 = new User("TestTransactional3", "TestTransactional3@domain.com", LocalDate.now());
        User test4 = new User("TestTransactional4", "TestTransactional4@domain.com", LocalDate.now());

        List<User> users = Arrays.asList(test1, test2, test3, test4);
        try {
            userService.saveTransactional(users);
        } catch (Exception e) {
            LOGGER.error("This is an error inside the method transactional, it rollback all the transaction");
        }

        userService.getAllUsers()
                .stream()
                .forEach(user -> LOGGER.info("Este es el usuario dentro del metodo transactional" + user));
    }

    private void getInformationJpqlFromUser() {
        LOGGER.info("Usuario con el metodo findByUserEmail" + userRepository.findByUserEmail("andres@gmail.com")
                .orElseThrow(() -> new RuntimeException("No se encontro el usuario")));

        userRepository.findAndSort("user", Sort.by("id").descending())
                .stream()
                .forEach(user -> LOGGER.info("Usuario con metodo sort" + user));

        userRepository.findByName("Andres")
                .stream()
                .forEach(user -> LOGGER.info("Usuario con query method" + user));

        LOGGER.info("Usuario con query method and email and name" + userRepository.findByEmailAndName("andres@gmail.com", "Andres")
                .orElseThrow(() -> new RuntimeException("No se encontro el usuario")));

        userRepository.findByNameLike("%user%")
                .stream()
                .forEach(user -> LOGGER.info("Usuario con query method like" + user));

        userRepository.findByNameOrEmail("Juan", "andres@gmail.com")
                .stream()
                .forEach(user -> LOGGER.info("Usuario con query method or" + user));

        userRepository.findByBirthDateBetween(LocalDate.of(2018, 1, 1), LocalDate.of(2019, 5, 21))
                .stream()
                .forEach(user -> LOGGER.info("Usuario con query method between" + user));

        userRepository.findByNameLikeOrderByIdDesc("%user%")
                .stream()
                .forEach(user -> LOGGER.info("Usuario con query method like order by" + user));

        LOGGER.info("El usuario a partir del named parameter es: " +
                userRepository.getAllByBirthDateAndEmail(LocalDate.of(2018, 1, 1), "user12@gmail.com")
                .orElseThrow(() -> new RuntimeException("No se encontro el usuario")));
    }

    private void saveUsersInDataBase() {
        User user1 = new User("Andres", "andres@gmail.com", LocalDate.of(2020, 10, 10));
        User user2 = new User("Juan", "juan@gmail.com", LocalDate.of(2010, 12, 12));
        User user3 = new User("Pedro", "pedro@gmail.com", LocalDate.of(2015, 1, 21));
        User user4 = new User("user4", "user4@gmail.com", LocalDate.of(2018, 1, 11));
        User user5 = new User("user5", "user5@gmail.com", LocalDate.of(2019, 5, 21));
        User user6 = new User("user6", "user6@gmail.com", LocalDate.of(2017, 7, 7));
        User user7 = new User("user7", "user7@gmail.com", LocalDate.of(2016, 11, 30));
        User user8 = new User("user8", "user8@gmail.com", LocalDate.of(2018, 1, 1));
        User user9 = new User("user9", "user9@gmail.com", LocalDate.of(2019, 5, 21));
        User user10 = new User("user10", "user10@gmail.com", LocalDate.of(2017, 7, 7));
        User user11 = new User("user11", "user11@gmail.com", LocalDate.of(2016, 11, 30));
        User user12 = new User("user12", "user12@gmail.com", LocalDate.of(2018, 1, 1));
        User user13 = new User("user13", "user13@gmail.com", LocalDate.of(2019, 5, 21));
        List<User> list = Arrays.asList(user1, user2, user3, user4, user5, user6, user7, user8, user9, user10, user11, user12, user13);
        userRepository.saveAll(list);
//        list.stream().forEach(userRepository::save);
//        list.forEach(userRepository::save);
    }

    private void ejemplosAnteriores(){
        componentDependency.saludar();
        myBean.print();
        myBeanWithDependency.printWithDependency();
        System.out.println(myOperation.sum(5));
        System.out.println(myBeanWithProperties.function());
        System.out.println(userPojo.getEmail() + " - " + userPojo.getPassword());
        try {
            int value = 10/0;
            LOGGER.debug("Mi valor: " + value);
        } catch (Exception e) {
            LOGGER.error("Esto es un error al dividir por cero" + e.getMessage());
        }
    }
}
