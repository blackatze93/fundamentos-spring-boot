package com.fundamentos.springboot.fundamentos.caseuse;

import com.fundamentos.springboot.fundamentos.entity.User;
import com.fundamentos.springboot.fundamentos.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class ListUser {
    private UserService userService;

    public ListUser(UserService userService) {
        this.userService = userService;
    }

    public Page<User> getAll(Pageable pageable) {
        return userService.findAll(pageable);
    }
}
